#Dwt-method3

 # import libraries 
import pandas as pd
 from PyEMD import EMD
 import numpy as np 
import pylab as plt
 import pywt

 def bin_power(X, Band, Fs): 
C = np.fft.fft(X) 
C = abs(C) 
Power = np.zeros(len(Band) - 1) 
for Freq_Index in range(0, len(Band) - 1): 
Freq = float(Band[Freq_Index]) 
Next_Freq = float(Band[Freq_Index + 1])
Power[Freq_Index] = sum(
 C[int(np.floor(Freq / Fs * len(X))):
 int(np.floor(Next_Freq / Fs * len(X)))]) 
Power_Ratio = Power / sum(Power) 
return Power, Power_Ratio 

#read file
df=pd.read_csv('als.csv') 
data=df['aa'][0:500].values 
time=df['time'][0:500].values 

# Execute EMD on signal
 IMF = EMD().emd(data,time) 
N = IMF.shape[0]+1 

# Plot results
plt.subplot(N,1,1) 
plt.plot(time, data, 'r') 
plt.title("Input signal") 
plt.xlabel("Time [s]") 
for n, imf in enumerate(IMF): 
plt.subplot(N,1,n+2) 
plt.plot(time, imf, 'g') 
plt.title("IMF "+str(n+1)) 
plt.xlabel("Time [s]")
plt.subplots_adjust(top=4,bottom=1,wspace=2,hspace=1.3
#plt.tight_layout()

 plt.show()
 # choose best imf i=int(N/2)

 #print(IMF[i])

 # perform Daubechies (db) wavelet transform
 cA, cD= pywt.dwt(IMF[i], 'db1') 
#cA=np.concatenate((cA,cD)) 
cA=cA.reshape(-1,1) 

# plot the wavelet
t=np.arange(0,len(cA),1)
plt.plot(t,cA) 
plt.xlabel('samples number') 
plt.ylabel('value') 
plt.title('obtained daubechies wavelet') 
plt.show() 

# feature extraction 

# bin power calculation
 fs=2*48
power,power_ratio=bin_power(cA,Band=[4,12,30,48],Fs=fs) print(power_ratio[1]) 
# result plot 
df1=pd.read_csv('method3.csv') 
x1=df1['normal'].values 
x2=df1['als'].values 

s1=np.arange(0,len(x1),1) 
s2=np.arange(0,1) 
fig, ax = plt.subplots() 
ax.plot(s1,x1, '>b', label='normal')
 ax.plot(s1,x2, '+g', label='ALS')
 ax.plot(s2, power_ratio[1], '*r', label='prediction') plt.ylabel('power ratio(beta)') 
plt.xlabel('samples') 
leg = ax.legend(loc=1)
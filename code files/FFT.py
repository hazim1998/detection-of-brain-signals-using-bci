#FFT METHOD

import numpy as np
import matplotlib.pylab as plt
import csv
import pandas as pd
from scipy import signal
import seaborn as sns
from scipy.integrate import simps

df = pd.read_csv('ALS2copy.csv')
r = df[['H']]
r.loc[:,'time'] = range(0,len(r))
r.plot(x='time',y = 'H',subplots=True,layout=(2,1),title='EEG data of normal person without scaling')
plt.show()
# fft using welch method
# sf:sampling frequency=2*fm
sf=100

# Define window length
#window should encompass min 2 full cycles at the lowest frequency 

# 12:min freq in hz
win = (2/12) * sf

# signal. used in signal processing
freqs, psd = signal.welch(r['H'], sf, nperseg=win)

# Plot the power spectrum
# seaborn is a data visualisation tool based on matplotlib
sns.set(font_scale=1.2, style='white')
plt.figure(figsize=(8, 4))
plt.plot(freqs, psd, color='b', lw=2)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power spectral density (V^2 / Hz)')
plt.ylim([0, psd.max() * 1.1])
plt.title("Welch's periodogram")
plt.xlim([0, freqs.max()])
sns.despine()

# Define beta lower and upper limits
low = 12.5
high = 30

# Find intersecting values in frequency vector
idx_beta = np.logical_and(freqs >= low, freqs <= high)

# Plot the power spectral density and fill the delta area
# plot the periodogram for specific freq range
plt.figure(figsize=(7, 4))
plt.plot(freqs, psd, lw=2, color='b')
plt.fill_between(freqs, psd, where=idx_beta, color='skyblue')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power spectral density (uV^2 / Hz)')
plt.xlim([0, 30])
plt.ylim([0, psd.max() * 1.1])
plt.title("Welch's periodogram")
# remove top and right spines from the plot
# sns.despine(fig=none,ax=none,top=true,right=false........)
sns.despine()

# Frequency resolution: difference in frequency between each bin
# frequency 'bin' is a segment that collects mag,amp or energy from small range of frequencies
freq_res = 6  # = 1 /(2/12) = 12/2 = 6

# Compute the absolute power by approximating the area under the curve
beta_power = simps(psd[idx_beta], dx=freq_res)
print('Absolute beta power: %.3f uV^2' % beta_power)

# Relative beta power (expressed as a percentage of total power)
total_power = simps(psd, dx=freq_res)
#simps: simpsons rule is method used for numerical approximation(using parabola-area under the cuve)
beta_rel_power = beta_power / total_power
print('Relative beta power: %.3f' % beta_rel_power)
print(beta_rel_power)
df1 = pd.read_csv('FIRST METHOD DATASET.csv')
x1 = df1[['RELATIVE BETA POWER (HEALTHY)','RELATIVE BETA POWER (ALS)']]
x1.loc[:,'s1'] = range(0,len(x1))
x1.loc[:,'s2'] = 0
x1.loc[:,'brl'] = beta_rel_power

# scatter plot
ax = x1.plot.scatter(x='s2',y ='brl',color='black',marker='^', label='prediction',figsize=(7,4))
x1.plot.scatter(x='s1',y ='RELATIVE BETA POWER (HEALTHY)',color='g',marker='d', label='NO ALS',ax=ax)
x1.plot.scatter(x='s1',y ='RELATIVE BETA POWER (ALS)',color='r',marker='x', label='ALS',ax=ax)
leg = ax.legend(loc=1)
plt.show()
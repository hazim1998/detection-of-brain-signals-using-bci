#Hht-method

 # import libraries 
import pandas as pd 
from PyEMD import EMD
 import numpy as np 
import matplotlib.pyplot as plt
 from scipy.signal import hilbert
 import cmath 
from scipy import signal
 from sklearn.neighbors import KNeighborsClassifier

 def spectral_entropy(x, sf):
 x = np.array(x)
 # Compute and normalize power spectrum 
win = (2/12) * sf 
freqs, psd = signal.welch(x, sf, nperseg=win) 
psd_norm = np.divide(psd, psd.sum()) 
se = -np.multiply(psd_norm, np.log2(psd_norm)).sum()
return se
 # data read
 df=pd.read_csv('normal.csv') 
data=df['ab'][0:254].values 
time=df['time'][0:254].values 

# Execute EMD on signal 
IMF = EMD().emd(data,time) 
N = IMF.shape[0]+1 

# Plot results
 plt.subplot(N,1,1) 
plt.plot(time, data, 'r') 
plt.title("Input signal") 
plt.xlabel("Time [s]") 
for n, imf in enumerate(IMF): 
plt.subplot(N,1,n+2)
 plt.plot(time, imf, 'g') 
plt.title("IMF "+str(n+1))
plt.xlabel("Time [s]")
plt.subplots_adjust(top=4,bottom=1,wspace=2,hspace=1.1) plt.show() 

# perform hilbert transform 
hil=hilbert(IMF[0], N=None, axis=-1) 

plt.plot(time,hil) 
plt.xlabel('time') 
plt.ylabel('transformed values') 
plt.title('Hilbert Transform') 
plt.show() 

# compute the spectral entropy of the signal se=spectral_entropy(hil,sf) 
sf=100 
print('The spectral entropy of Beta band is:',se) 

df1=pd.read_csv('method4.csv')
 x = df1['se'].values.reshape(-1,1)
 y = df1['class_se'].values 

#classifier 1 
neigh = KNeighborsClassifier(n_neighbors=3) 
neigh.fit(x, y) 
res=neigh.predict([[se]]) 
if res==0:
print('\n') 
print('NORMAL') 
else:
print('\n') 
print('ALS')
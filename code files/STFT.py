#Stft-method2 

# importing the required libraries 
import csv 
import numpy as np 
import matplotlib.pylab as plt
 from scipy import signal 
from scipy.integrate import simps 
from sklearn import svm
 from sklearn.neighbors import KNeighborsClassifier
 from sklearn import metrics 
import pandas as pd

# acquire the data
 df=pd.read_csv('normal.csv')
t=df['time'].values
 data=df['t'].values

 # plotting
 plt.figure(figsize=(15,7.5), dpi=70, facecolor='w', edgecolor='b') plt.plot(t, data, color='orange') 
plt.xlabel('Time', fontsize=20, color='b')
 plt.ylabel('Action Potential',fontsize=20, color='b') 
plt.title('EEG Raw data', fontsize=20, color='b')
 plt.show()

# define window size
 # maximum frequency component=48hz (gamma wave) fmax=48
 fs=2*fmax
 win=2/12.5*fs

 # stft
 # scipy.signal.stft(x, fs=1.0, window='hann', nperseg=256, noverlap=None, nfft=None, detrend=False, return_onesided=True, boundary='zeros', padded=True, axis=-1) f, t, Zxx = signal.stft(data, fs, window='hanning', nperseg=win) plt.pcolormesh(t, f, np.abs(Zxx), vmin=-70, vmax=40) plt.title('STFT Magnitude') plt.ylabel('Frequency [Hz]') plt.xlabel('Time [sec]') 
plt.show()

# beta wave
 plt.plot(t, abs(Zxx[3]))
 plt.xlabel('Time') 
plt.ylabel('Beta amplitude') 
plt.title('Beta wave') 
plt.show()

 # rms analysis
 v_peak = max(abs(Zxx[3]))
 v_rms = 0.7071*v_peak
 print('Vrms:%0.3f mV'%v_rms)

 freqs, psd = signal.welch(abs(Zxx[3]), fs, nperseg=win) 
# Define beta lower and upper limits
 low = 12.5
 high = 30
 # Find intersecting values in frequency vector
 idx_beta = np.logical_and(freqs >= low, freqs <= high) 

# Frequency resolution: difference in frequency between each bin
 # frequency 'bin' is a segment that collects mag,amp or energy from small range of frequencies 
freq_res = 6 # = 1 /(2/12) = 12/2 = 6

 # Compute the absolute power by approximating the area under the curve
 beta_power = simps(psd[idx_beta], dx=freq_res)

 # Relative power (expressed as a percentage of total power) total_power = simps(psd, dx=freq_res) 
beta_rel_power = (beta_power / total_power) 
print('Relative beta power: %.3f' % beta_rel_power) 

win_2=2/5*fs 
low_1=5
 high_1=12.8 
freqs_1, psd_1 = signal.welch(abs(Zxx[2]), fs, nperseg=win_2) idx_alpha = np.logical_and(freqs >= low_1, freqs <= high_1) freq_res_1=2.5
 alpha_power = simps(psd[idx_alpha], dx=freq_res) total_power_1 = simps(psd, dx=freq_res) alpha_rel_power = (alpha_power / total_power_1)
 print('Relative alpha power: %.3f' % alpha_rel_power) 

# relation between alpha and beta rel=beta_rel_power/alpha_rel_power
 print('relative ratio between beta/alpha:%0.3f'%rel) 

# classification 
df1=pd.read_csv('method2data.csv')

 x1=df1['vrms(mv)'].values.reshape(-1,1) x2=df1['beta/alpha'].values.reshape(-1,1) x=np.concatenate((x1,x2),axis=1).reshape(-1,2) y_train=df1['class']

 # start svm
 cls=svm.SVC(kernel='rbf',gamma=3, C=0.5, random_state=0) 

# train the network 
cls.fit(x,y_train) 

# prediction 
X_pred=[[v_rms, rel]]
 pred=cls.predict(X_pred)

 if pred==0: 
print('\n')
print('SVM RESULT') 
print('UNAFFECTED') 
else:
 print('\n') 
print('SVM RESULT') 
print('AFFECTED') 

# KNN (rms)
 x1_train=x1 
print(x1_train)
 neigh = KNeighborsClassifier(n_neighbors=3) neigh.fit(x1_train,y_train) 
pre = neigh.predict([[v_rms]])
 if pre==0:
 print('\n') 
print('KNN RESULT:1') 
print('UNAFFECTED') 
else:
print('\n') 
print('KNN RESULT:1') 
print('AFFECTED') 

# KNN (ratio)
 x2_train=x2 

neigh = KNeighborsClassifier(n_neighbors=3) neigh.fit(x2_train,y_train)
 pre = neigh.predict([[rel]])


 if pre==0:
 print('\n') 
print('KNN RESULT:2') 
print('UNAFFECTED') 
else: 
print('\n') 
print('KNN RESULT:2') 
print('AFFECTED') 